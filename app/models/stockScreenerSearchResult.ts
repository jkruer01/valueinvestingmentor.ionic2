import { LinearRegression } from './linearRegression';

export class StockScreenerSearchResult {
    id: string;
    stockSymbol: string;
    name: string;
    fiveYearAnalystGrowthEstimate: number;
    historicalPeRatio: number;
    lastUpdatedOn: string;
    createdOn: string;
    lastPrice: number;
    mosPrice: number;
    paybackTime: any;
    growthRate: number;
    predictabilityRating: number;
    linearRegression: LinearRegression;
    marginOfSafety: number;
    numberOfFinancialStatements: number;
}