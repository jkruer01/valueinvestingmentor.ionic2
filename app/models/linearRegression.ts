export class LinearRegression {
    rSquared: number;
    yIntercept: number;
    slope: number;
}