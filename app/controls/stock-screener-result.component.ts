import {Component, Input, OnInit} from '@angular/core';
import {StockScreenerSearchResult} from '../models/StockScreenerSearchResult';

@Component({
    selector: 'stock-screener-result',
    templateUrl: 'build/controls/stock-screener-result.component.html'
})
export class StockScreenerResultComponent implements OnInit {
    @Input()
    searchResult: StockScreenerSearchResult;
    
    isLastPriceGood = false;
    isLastPriceBad = false;
    isLastPriceWarning = false;
    
    isPaybackTimeGood = false;
    isPaybackTimeBad = false;
    isPaybackTimeWarning = false;
    
    isGrowthRateGood = false;
    isGrowthRateBad = false;
    isGrowthRateWarning = false;

    constructor() {        
    }

    ngOnInit(): void {
        this.calculateLastPriceInfo();
        this.calculatePaybackTimeInfo();
        this.calculateGrowthRateInfo();
    }

    calculateLastPriceInfo(): void {
        let warningPrice = this.searchResult.mosPrice * 1.2;

        if (this.searchResult.lastPrice <= this.searchResult.mosPrice) {
            this.isLastPriceGood = true;
        }
        else if (this.searchResult.lastPrice <= warningPrice) {
            this.isLastPriceWarning = true;
        }
        else {
            this.isLastPriceBad = true;
        }
    }

    calculatePaybackTimeInfo(): void {
        if (this.searchResult.paybackTime <= 8) {
            this.isPaybackTimeGood = true;
        }
        else if (this.searchResult.paybackTime <= 9) {
            this.isPaybackTimeWarning = true;
        }
        else {
            this.isPaybackTimeBad = true;
        }
    }

    calculateGrowthRateInfo(): void {
        if (this.searchResult.growthRate >= 10) {
            this.isGrowthRateGood = true;
        }
        else if (this.searchResult.growthRate > 5) {
            this.isGrowthRateWarning = true;
        }
        else {
            this.isGrowthRateBad = true;
        }
    }
}