import {Injectable} from '@angular/core';
import { StockScreenerSearchResult } from '../models/StockScreenerSearchResult';
import { STOCKSCREENERRESULTS } from './mock-StockScreenerResults';

@Injectable()
export class StockScreenerService {
    getUnderValuedStocks(): StockScreenerSearchResult[] {
        return STOCKSCREENERRESULTS;
    }
}