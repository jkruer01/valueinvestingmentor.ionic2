import { StockScreenerSearchResult } from '../models/stockScreenerSearchResult'

export const STOCKSCREENERRESULTS: StockScreenerSearchResult[] = [{
		"id" : "000000000000000000000000",
		"stockSymbol" : "AER",
		"name" : "Aercap Holdings N.V. Ordinary S",
		"fiveYearAnalystGrowthEstimate" : 10.0,
		"historicalPeRatio" : 10.5,
		"lastUpdatedOn" : "2016-09-11T02:17:10.964Z",
		"createdOn" : "2015-06-01T13:23:23.374Z",
		"lastPrice" : 39.32,
		"mosPrice" : 33.15,
		"paybackTime" : 19.48,
		"growthRate" : 14.01,
		"predictabilityRating" : 42.022530321297829,
		"linearRegression" : {
			"rSquared" : 21.0796,
			"yIntercept" : 20.0257,
			"slope" : 0.0102
		},
		"marginOfSafety" : -15.69,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "AFSI",
		"name" : "AmTrust Financial Services, Inc",
		"fiveYearAnalystGrowthEstimate" : 12.0,
		"historicalPeRatio" : 10.4,
		"lastUpdatedOn" : "2016-09-11T02:22:33.726Z",
		"createdOn" : "2015-06-01T13:26:25.505Z",
		"lastPrice" : 26.36,
		"mosPrice" : 35.74,
		"paybackTime" : 13.67,
		"growthRate" : 25.13,
		"predictabilityRating" : 66.185031315364824,
		"linearRegression" : {
			"rSquared" : 0.431,
			"yIntercept" : 21.2006,
			"slope" : -0.0193
		},
		"marginOfSafety" : 35.58,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "AHGP",
		"name" : "Alliance Holdings GP, L.P.",
		"fiveYearAnalystGrowthEstimate" : 14.000000000000002,
		"historicalPeRatio" : 14.1,
		"lastUpdatedOn" : "2016-09-11T02:28:03.115Z",
		"createdOn" : "2015-06-01T13:29:38.059Z",
		"lastPrice" : 25.3,
		"mosPrice" : 22.44,
		"paybackTime" : 9.55,
		"growthRate" : 13.96,
		"predictabilityRating" : 70.164129408652045,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : -11.3,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "AL",
		"name" : "Air Lease Corporation Class A C",
		"fiveYearAnalystGrowthEstimate" : 15.0,
		"historicalPeRatio" : 0.0,
		"lastUpdatedOn" : "2016-09-11T03:56:49.881Z",
		"createdOn" : "2015-06-01T13:34:39.903Z",
		"lastPrice" : 28.75,
		"mosPrice" : 47.1,
		"paybackTime" : "NaN",
		"growthRate" : 36.47,
		"predictabilityRating" : 42.583953027050008,
		"linearRegression" : {
			"rSquared" : 30.908,
			"yIntercept" : 10.7871,
			"slope" : -0.0471
		},
		"marginOfSafety" : 63.83,
		"numberOfFinancialStatements" : 25
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "AMCN",
		"name" : "AirMedia Group Inc",
		"fiveYearAnalystGrowthEstimate" : 20.0,
		"historicalPeRatio" : 0.0,
		"lastUpdatedOn" : "2016-08-09T06:05:29.283Z",
		"createdOn" : "2015-06-01T13:41:10.506Z",
		"lastPrice" : 3.075,
		"mosPrice" : 71.02,
		"paybackTime" : "Infinity",
		"growthRate" : 46.23,
		"predictabilityRating" : -63.189527209649981,
		"linearRegression" : {
			"rSquared" : 20.5201,
			"yIntercept" : 17.2894,
			"slope" : -0.001
		},
		"marginOfSafety" : 2209.59,
		"numberOfFinancialStatements" : 41
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "ANFI",
		"name" : "Amira Nature Foods Ltd Ordinary",
		"fiveYearAnalystGrowthEstimate" : 25.0,
		"historicalPeRatio" : 0.0,
		"lastUpdatedOn" : "2016-09-11T04:48:07.689Z",
		"createdOn" : "2015-06-01T13:48:39.732Z",
		"lastPrice" : 7.2,
		"mosPrice" : 58.85,
		"paybackTime" : "NaN",
		"growthRate" : 23.96,
		"predictabilityRating" : 3.3030841457997582,
		"linearRegression" : {
			"rSquared" : 5.5194,
			"yIntercept" : 4.9037,
			"slope" : 0.0064
		},
		"marginOfSafety" : 717.36,
		"numberOfFinancialStatements" : 20
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "AOI",
		"name" : "Alliance One International, Inc",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 13.2,
		"lastUpdatedOn" : "2016-09-11T04:57:01.396Z",
		"createdOn" : "2015-06-01T13:50:47.125Z",
		"lastPrice" : 20.71,
		"mosPrice" : 109.78,
		"paybackTime" : 5.17,
		"growthRate" : 15.63,
		"predictabilityRating" : -66.354993847038145,
		"linearRegression" : {
			"rSquared" : 5.8682,
			"yIntercept" : 20.4594,
			"slope" : 0.002
		},
		"marginOfSafety" : 430.08,
		"numberOfFinancialStatements" : 50
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "ASPS",
		"name" : "Altisource Portfolio Solutions",
		"fiveYearAnalystGrowthEstimate" : 20.0,
		"historicalPeRatio" : 17.1,
		"lastUpdatedOn" : "2016-09-11T05:40:21.624Z",
		"createdOn" : "2015-06-01T14:05:42.586Z",
		"lastPrice" : 32.21,
		"mosPrice" : 45.0,
		"paybackTime" : 6.79,
		"growthRate" : 31.49,
		"predictabilityRating" : 64.156023609562226,
		"linearRegression" : {
			"rSquared" : 59.5197,
			"yIntercept" : 16.4505,
			"slope" : -0.1055
		},
		"marginOfSafety" : 39.71,
		"numberOfFinancialStatements" : 34
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "BCOM",
		"name" : "B Communications Ltd.",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 18.3,
		"lastUpdatedOn" : "2016-09-11T06:28:17.881Z",
		"createdOn" : "2015-06-01T14:38:08.314Z",
		"lastPrice" : 23.84,
		"mosPrice" : 75.78,
		"paybackTime" : 1.15,
		"growthRate" : 24.67,
		"predictabilityRating" : -104.40373879210668,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 217.87,
		"numberOfFinancialStatements" : 47
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "BFR",
		"name" : "BBVA Banco Frances S.A. Common",
		"fiveYearAnalystGrowthEstimate" : 27.11,
		"historicalPeRatio" : 5.8,
		"lastUpdatedOn" : "2016-09-11T06:40:46.278Z",
		"createdOn" : "2015-06-01T14:45:39.274Z",
		"lastPrice" : 19.69,
		"mosPrice" : 163.8,
		"paybackTime" : 2.64,
		"growthRate" : 29.71,
		"predictabilityRating" : 53.036248532000215,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 731.89,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "BIDU",
		"name" : "Baidu, Inc.",
		"fiveYearAnalystGrowthEstimate" : 27.25,
		"historicalPeRatio" : 53.8,
		"lastUpdatedOn" : "2015-07-01T16:37:15.659Z",
		"createdOn" : "2015-06-01T14:49:59.368Z",
		"lastPrice" : 196.39,
		"mosPrice" : 446.15,
		"paybackTime" : 15.0,
		"growthRate" : 95.81,
		"predictabilityRating" : 14.646487287574217,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 127.18,
		"numberOfFinancialStatements" : 44
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "BKJ",
		"name" : "Bancorp of New Jersey, Inc Comm",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 16.3,
		"lastUpdatedOn" : "2016-09-11T06:53:57.227Z",
		"createdOn" : "2015-06-01T14:54:03.466Z",
		"lastPrice" : 11.2133,
		"mosPrice" : 10.06,
		"paybackTime" : 24.22,
		"growthRate" : 14.2,
		"predictabilityRating" : 63.050927660167247,
		"linearRegression" : {
			"rSquared" : 1.2495,
			"yIntercept" : 16.6966,
			"slope" : -0.0022
		},
		"marginOfSafety" : -10.29,
		"numberOfFinancialStatements" : 41
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "BKSC",
		"name" : "Bank of South Carolina Corp.",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 15.7,
		"lastUpdatedOn" : "2016-09-11T06:54:54.016Z",
		"createdOn" : "2015-06-01T14:54:42.205Z",
		"lastPrice" : 17.31,
		"mosPrice" : 15.13,
		"paybackTime" : 17.94,
		"growthRate" : 15.19,
		"predictabilityRating" : 52.715576450699849,
		"linearRegression" : {
			"rSquared" : 5.6269,
			"yIntercept" : 21.7643,
			"slope" : -0.0485
		},
		"marginOfSafety" : -12.59,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "BMA",
		"name" : "Banco Macro S.A.  ADR (represen",
		"fiveYearAnalystGrowthEstimate" : 21.040001999999998,
		"historicalPeRatio" : 5.6,
		"lastUpdatedOn" : "2016-09-11T06:59:38.238Z",
		"createdOn" : "2015-06-01T14:57:59.993Z",
		"lastPrice" : 74.59,
		"mosPrice" : 227.2,
		"paybackTime" : 9.71,
		"growthRate" : 17.99,
		"predictabilityRating" : 35.422689604548808,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 204.6,
		"numberOfFinancialStatements" : 48
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "BOFI",
		"name" : "BofI Holding, Inc.",
		"fiveYearAnalystGrowthEstimate" : 10.0,
		"historicalPeRatio" : 15.5,
		"lastUpdatedOn" : "2016-09-11T07:04:32.915Z",
		"createdOn" : "2015-06-01T15:01:09.511Z",
		"lastPrice" : 21.35,
		"mosPrice" : 19.62,
		"paybackTime" : 19.98,
		"growthRate" : 14.13,
		"predictabilityRating" : 41.877388471043844,
		"linearRegression" : {
			"rSquared" : 2.2279,
			"yIntercept" : 19.2932,
			"slope" : -0.0018
		},
		"marginOfSafety" : -8.1,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "BRT",
		"name" : "BRT Realty Trust Common Stock",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 38.1,
		"lastUpdatedOn" : "2016-09-11T07:16:51.128Z",
		"createdOn" : "2015-06-01T15:09:42.131Z",
		"lastPrice" : 8.01,
		"mosPrice" : 23.74,
		"paybackTime" : "NaN",
		"growthRate" : 12.8,
		"predictabilityRating" : -50.776347115025459,
		"linearRegression" : {
			"rSquared" : 4.4335,
			"yIntercept" : 19.805,
			"slope" : 0.0071
		},
		"marginOfSafety" : 196.38,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "CALM",
		"name" : "Cal-Maine Foods, Inc.",
		"fiveYearAnalystGrowthEstimate" : 67.0,
		"historicalPeRatio" : 14.8,
		"lastUpdatedOn" : "2016-09-11T07:39:50.698Z",
		"createdOn" : "2015-06-01T15:25:24.181Z",
		"lastPrice" : 43.46,
		"mosPrice" : 432.32,
		"paybackTime" : 8.29,
		"growthRate" : 26.18,
		"predictabilityRating" : 39.057422338090085,
		"linearRegression" : {
			"rSquared" : 0.0825,
			"yIntercept" : 20.6153,
			"slope" : -0.0025
		},
		"marginOfSafety" : 894.75,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "CAP",
		"name" : "Cai International, Inc. Common",
		"fiveYearAnalystGrowthEstimate" : 9.0,
		"historicalPeRatio" : 9.6,
		"lastUpdatedOn" : "2015-09-10T22:04:10.773Z",
		"createdOn" : "2015-06-01T15:26:36.5Z",
		"lastPrice" : 12.99,
		"mosPrice" : 14.9,
		"paybackTime" : "NaN",
		"growthRate" : 12.18,
		"predictabilityRating" : 26.517894827849588,
		"linearRegression" : {
			"rSquared" : 1.2509,
			"yIntercept" : 14.8902,
			"slope" : -0.0102
		},
		"marginOfSafety" : 14.7,
		"numberOfFinancialStatements" : 40
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "CCSC",
		"name" : "Country Style Cooking Restauran",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 31.8,
		"lastUpdatedOn" : "2016-09-11T08:04:37.792Z",
		"createdOn" : "2015-06-01T15:41:55.499Z",
		"lastPrice" : 5.17,
		"mosPrice" : 10.74,
		"paybackTime" : 10.65,
		"growthRate" : 26.3,
		"predictabilityRating" : -23.780612898029247,
		"linearRegression" : {
			"rSquared" : 4.4665,
			"yIntercept" : 9.1856,
			"slope" : -0.0164
		},
		"marginOfSafety" : 107.74,
		"numberOfFinancialStatements" : 27
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "CEA",
		"name" : "China Eastern Airlines Corporat",
		"fiveYearAnalystGrowthEstimate" : 57.84,
		"historicalPeRatio" : 11.7,
		"lastUpdatedOn" : "2016-09-11T08:09:52.968Z",
		"createdOn" : "2015-06-01T15:44:32.583Z",
		"lastPrice" : 26.29,
		"mosPrice" : 30.34,
		"paybackTime" : 10.01,
		"growthRate" : 13.83,
		"predictabilityRating" : -10.263789484257131,
		"linearRegression" : {
			"rSquared" : 0.7963,
			"yIntercept" : 20.716,
			"slope" : -0.0013
		},
		"marginOfSafety" : 15.41,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "CELG",
		"name" : "Celgene Corporation",
		"fiveYearAnalystGrowthEstimate" : 21.87,
		"historicalPeRatio" : 37.9,
		"lastUpdatedOn" : "2016-09-11T08:12:07.546Z",
		"createdOn" : "2015-06-01T15:45:49.892Z",
		"lastPrice" : 104.47,
		"mosPrice" : 92.99,
		"paybackTime" : 16.9,
		"growthRate" : 22.39,
		"predictabilityRating" : 49.699233926982117,
		"linearRegression" : {
			"rSquared" : 5.1484,
			"yIntercept" : 20.6659,
			"slope" : 0.0181
		},
		"marginOfSafety" : -10.99,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "CGA",
		"name" : "China Green Agriculture, Inc. C",
		"fiveYearAnalystGrowthEstimate" : 12.5,
		"historicalPeRatio" : 2.8,
		"lastUpdatedOn" : "2016-09-11T08:20:02.596Z",
		"createdOn" : "2015-06-01T15:50:22.818Z",
		"lastPrice" : 1.4401,
		"mosPrice" : 7.73,
		"paybackTime" : 4.99,
		"growthRate" : 19.51,
		"predictabilityRating" : 41.214757641645832,
		"linearRegression" : {
			"rSquared" : 3.0245,
			"yIntercept" : 14.8269,
			"slope" : -0.0032
		},
		"marginOfSafety" : 436.77,
		"numberOfFinancialStatements" : 39
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "CIG.C",
		"name" : "Comp En De Mn Cemig ADS America",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 7.5,
		"lastUpdatedOn" : "2016-09-11T08:36:52.026Z",
		"createdOn" : "2015-06-07T02:52:43.76Z",
		"lastPrice" : 2.81,
		"mosPrice" : 8.03,
		"paybackTime" : 7.4,
		"growthRate" : 24.97,
		"predictabilityRating" : 20.046425596576853,
		"linearRegression" : {
			"rSquared" : 15.573,
			"yIntercept" : 24.63,
			"slope" : -0.0008
		},
		"marginOfSafety" : 185.77,
		"numberOfFinancialStatements" : 54
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "CNTF",
		"name" : "China TechFaith Wireless Commun",
		"fiveYearAnalystGrowthEstimate" : 25.0,
		"historicalPeRatio" : 6.1,
		"lastUpdatedOn" : "2016-09-11T09:10:35.584Z",
		"createdOn" : "2015-06-01T16:13:46.516Z",
		"lastPrice" : 2.69,
		"mosPrice" : 5.88,
		"paybackTime" : "Infinity",
		"growthRate" : 18.99,
		"predictabilityRating" : -54.759220138898343,
		"linearRegression" : {
			"rSquared" : 1.0972,
			"yIntercept" : 19.6613,
			"slope" : 0.0054
		},
		"marginOfSafety" : 118.59,
		"numberOfFinancialStatements" : 48
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "CPA",
		"name" : "Copa Holdings, S.A. Copa Holdin",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 12.1,
		"lastUpdatedOn" : "2015-07-01T16:42:49.448Z",
		"createdOn" : "2015-06-01T16:20:13.655Z",
		"lastPrice" : 82.555,
		"mosPrice" : 78.14,
		"paybackTime" : 31.41,
		"growthRate" : 12.7,
		"predictabilityRating" : 41.522517195855791,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : -5.35,
		"numberOfFinancialStatements" : 44
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "CREG",
		"name" : "China Recycling Energy Corporat",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 7.4,
		"lastUpdatedOn" : "2016-09-11T09:39:32.883Z",
		"createdOn" : "2015-06-01T16:26:26.826Z",
		"lastPrice" : 1.9,
		"mosPrice" : 7.53,
		"paybackTime" : "Infinity",
		"growthRate" : 17.86,
		"predictabilityRating" : -6.3960614994624763,
		"linearRegression" : {
			"rSquared" : 6.58,
			"yIntercept" : 20.4049,
			"slope" : 0.013
		},
		"marginOfSafety" : 296.32,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "CRH",
		"name" : "CRH PLC American Depositary Sha",
		"fiveYearAnalystGrowthEstimate" : 37.9,
		"historicalPeRatio" : 24.7,
		"lastUpdatedOn" : "2016-09-11T09:40:24.625Z",
		"createdOn" : "2015-06-01T16:26:52.978Z",
		"lastPrice" : 32.32,
		"mosPrice" : 123.32,
		"paybackTime" : 16.27,
		"growthRate" : 17.04,
		"predictabilityRating" : 57.874138676247341,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 281.56,
		"numberOfFinancialStatements" : 60
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "CRUS",
		"name" : "Cirrus Logic, Inc.",
		"fiveYearAnalystGrowthEstimate" : 20.0,
		"historicalPeRatio" : 17.0,
		"lastUpdatedOn" : "2016-09-11T09:46:56.033Z",
		"createdOn" : "2015-06-01T16:29:56.684Z",
		"lastPrice" : 48.45,
		"mosPrice" : 49.28,
		"paybackTime" : 20.24,
		"growthRate" : 22.02,
		"predictabilityRating" : 32.533474870962749,
		"linearRegression" : {
			"rSquared" : 1.775,
			"yIntercept" : 20.0759,
			"slope" : 0.0086
		},
		"marginOfSafety" : 1.71,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "CSIQ",
		"name" : "Canadian Solar Inc.",
		"fiveYearAnalystGrowthEstimate" : 20.0,
		"historicalPeRatio" : 12.1,
		"lastUpdatedOn" : "2016-03-04T05:20:44.531Z",
		"createdOn" : "2015-06-01T16:33:25.423Z",
		"lastPrice" : 22.4949,
		"mosPrice" : 96.73,
		"paybackTime" : 10.49,
		"growthRate" : 34.1,
		"predictabilityRating" : -59.77296309977217,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 330.01,
		"numberOfFinancialStatements" : 46
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "CSRE",
		"name" : "CSR plc",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 0.0,
		"lastUpdatedOn" : "2016-09-11T09:55:26.43Z",
		"createdOn" : "2015-06-01T16:34:01.088Z",
		"lastPrice" : 56.18,
		"mosPrice" : 50.23,
		"paybackTime" : 26.43,
		"growthRate" : 17.86,
		"predictabilityRating" : 1.6492881781071418,
		"linearRegression" : {
			"rSquared" : 0.004,
			"yIntercept" : 7.9863,
			"slope" : 0.0004
		},
		"marginOfSafety" : -10.59,
		"numberOfFinancialStatements" : 25
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "CTRX",
		"name" : "Catamaran Corporation",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 0.0,
		"lastUpdatedOn" : "2016-09-11T10:03:38.082Z",
		"createdOn" : "2015-06-01T16:38:05.298Z",
		"lastPrice" : 61.47,
		"mosPrice" : 3038.05,
		"paybackTime" : 6.64,
		"growthRate" : 61.7,
		"predictabilityRating" : 20.37805054786196,
		"linearRegression" : {
			"rSquared" : 21.7636,
			"yIntercept" : 21.3872,
			"slope" : -0.0341
		},
		"marginOfSafety" : 4842.33,
		"numberOfFinancialStatements" : 44
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "CYOU",
		"name" : "Changyou.com Limited",
		"fiveYearAnalystGrowthEstimate" : 487.74,
		"historicalPeRatio" : 10.7,
		"lastUpdatedOn" : "2016-03-04T05:40:34.72Z",
		"createdOn" : "2015-06-01T16:49:01.484Z",
		"lastPrice" : 17.88,
		"mosPrice" : 151.91,
		"paybackTime" : "Infinity",
		"growthRate" : 23.47,
		"predictabilityRating" : 62.137389008269508,
		"linearRegression" : {
			"rSquared" : 77.47,
			"yIntercept" : 15.0163,
			"slope" : -0.1282
		},
		"marginOfSafety" : 749.61,
		"numberOfFinancialStatements" : 33
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "DHIL",
		"name" : "Diamond Hill Investment Group,",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 16.2,
		"lastUpdatedOn" : "2016-09-11T10:51:21.607Z",
		"createdOn" : "2015-06-01T17:01:26.557Z",
		"lastPrice" : 186.11,
		"mosPrice" : 1282.72,
		"paybackTime" : 9.23,
		"growthRate" : 31.23,
		"predictabilityRating" : 53.728502854868111,
		"linearRegression" : {
			"rSquared" : 24.366,
			"yIntercept" : 23.5072,
			"slope" : -0.0565
		},
		"marginOfSafety" : 589.23,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "DISCA",
		"name" : "Discovery Communications, Inc.",
		"fiveYearAnalystGrowthEstimate" : 15.7,
		"historicalPeRatio" : 40.6,
		"lastUpdatedOn" : "2016-09-11T10:54:25.528Z",
		"createdOn" : "2015-06-01T17:02:51.127Z",
		"lastPrice" : 24.55,
		"mosPrice" : 30.86,
		"paybackTime" : 17.47,
		"growthRate" : 16.71,
		"predictabilityRating" : 50.380556744704222,
		"linearRegression" : {
			"rSquared" : 0.2493,
			"yIntercept" : 19.0159,
			"slope" : -0.0014
		},
		"marginOfSafety" : 25.7,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "DISCB",
		"name" : "Discovery Communications, Inc.",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 40.9,
		"lastUpdatedOn" : "2016-09-11T10:54:51.561Z",
		"createdOn" : "2015-06-01T17:03:03.28Z",
		"lastPrice" : 27.25,
		"mosPrice" : 35.83,
		"paybackTime" : 17.17,
		"growthRate" : 16.71,
		"predictabilityRating" : 50.380556744704222,
		"linearRegression" : {
			"rSquared" : 0.2493,
			"yIntercept" : 19.0159,
			"slope" : -0.0014
		},
		"marginOfSafety" : 31.49,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "DISCK",
		"name" : "Discovery Communications, Inc.",
		"fiveYearAnalystGrowthEstimate" : 15.97,
		"historicalPeRatio" : 21.4,
		"lastUpdatedOn" : "2016-09-11T10:55:17.434Z",
		"createdOn" : "2015-06-01T17:03:15.185Z",
		"lastPrice" : 23.67,
		"mosPrice" : 32.13,
		"paybackTime" : 16.95,
		"growthRate" : 16.71,
		"predictabilityRating" : 50.380556744704222,
		"linearRegression" : {
			"rSquared" : 0.2493,
			"yIntercept" : 19.0159,
			"slope" : -0.0014
		},
		"marginOfSafety" : 35.74,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "DLTR",
		"name" : "Dollar Tree, Inc.",
		"fiveYearAnalystGrowthEstimate" : 19.07,
		"historicalPeRatio" : 23.0,
		"lastUpdatedOn" : "2016-09-11T11:00:21.891Z",
		"createdOn" : "2015-06-01T17:05:34.323Z",
		"lastPrice" : 81.63,
		"mosPrice" : 74.78,
		"paybackTime" : 18.54,
		"growthRate" : 18.44,
		"predictabilityRating" : 79.735400725249121,
		"linearRegression" : {
			"rSquared" : 3.8326,
			"yIntercept" : 23.7725,
			"slope" : -0.1604
		},
		"marginOfSafety" : -8.39,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "DOOR",
		"name" : "Masonite International Corporat",
		"fiveYearAnalystGrowthEstimate" : 50.0,
		"historicalPeRatio" : 0.0,
		"lastUpdatedOn" : "2016-09-11T11:04:47.972Z",
		"createdOn" : "2015-06-01T17:07:41.178Z",
		"lastPrice" : 62.74,
		"mosPrice" : 252.07,
		"paybackTime" : 14.77,
		"growthRate" : 36.86,
		"predictabilityRating" : 0.33119704630628632,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 301.77,
		"numberOfFinancialStatements" : 23
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "DRII",
		"name" : "Diamond Resorts International,",
		"fiveYearAnalystGrowthEstimate" : 54.37,
		"historicalPeRatio" : 0.0,
		"lastUpdatedOn" : "2016-09-11T11:12:10.265Z",
		"createdOn" : "2015-06-01T17:11:03.21Z",
		"lastPrice" : 30.22,
		"mosPrice" : 117.44,
		"paybackTime" : 11.89,
		"growthRate" : 27.51,
		"predictabilityRating" : -178.89458237744094,
		"linearRegression" : {
			"rSquared" : 33.3782,
			"yIntercept" : 4.0869,
			"slope" : 0.0076
		},
		"marginOfSafety" : 288.62,
		"numberOfFinancialStatements" : 20
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "DTV",
		"name" : "DIRECTV",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 21.9,
		"lastUpdatedOn" : "2016-09-11T11:19:28.579Z",
		"createdOn" : "2015-06-01T17:14:52.808Z",
		"lastPrice" : 93.55,
		"mosPrice" : 156.87,
		"paybackTime" : 15.44,
		"growthRate" : 19.12,
		"predictabilityRating" : 60.327269922972206,
		"linearRegression" : {
			"rSquared" : 0.1315,
			"yIntercept" : 17.8937,
			"slope" : 0.0025
		},
		"marginOfSafety" : 67.69,
		"numberOfFinancialStatements" : 44
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "ECPG",
		"name" : "Encore Capital Group Inc",
		"fiveYearAnalystGrowthEstimate" : 15.0,
		"historicalPeRatio" : 12.0,
		"lastUpdatedOn" : "2016-09-11T11:35:05.729Z",
		"createdOn" : "2015-06-01T17:22:39.002Z",
		"lastPrice" : 22.16,
		"mosPrice" : 24.11,
		"paybackTime" : 13.59,
		"growthRate" : 14.79,
		"predictabilityRating" : 81.095415247212529,
		"linearRegression" : {
			"rSquared" : 0.7263,
			"yIntercept" : 21.5767,
			"slope" : -0.0605
		},
		"marginOfSafety" : 8.8,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "EPAM",
		"name" : "EPAM Systems, Inc. Common Stock",
		"fiveYearAnalystGrowthEstimate" : 20.0,
		"historicalPeRatio" : 0.0,
		"lastUpdatedOn" : "2016-09-11T12:08:44.02Z",
		"createdOn" : "2015-06-01T17:39:57.384Z",
		"lastPrice" : 68.09,
		"mosPrice" : 57.55,
		"paybackTime" : 20.05,
		"growthRate" : 31.2,
		"predictabilityRating" : 34.450325238866952,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : -15.48,
		"numberOfFinancialStatements" : 27
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "ESI",
		"name" : "ITT Educational Services, Inc.",
		"fiveYearAnalystGrowthEstimate" : 10.0,
		"historicalPeRatio" : 5.7,
		"lastUpdatedOn" : "2016-09-11T12:20:42.03Z",
		"createdOn" : "2015-06-01T17:46:12.495Z",
		"lastPrice" : 0.358,
		"mosPrice" : 4.51,
		"paybackTime" : -19.04,
		"growthRate" : 8.98,
		"predictabilityRating" : 62.947396203168132,
		"linearRegression" : {
			"rSquared" : 24.2226,
			"yIntercept" : 22.1758,
			"slope" : -0.1146
		},
		"marginOfSafety" : 1159.78,
		"numberOfFinancialStatements" : 51
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "ESRX",
		"name" : "Express Scripts Holding Company",
		"fiveYearAnalystGrowthEstimate" : 15.07,
		"historicalPeRatio" : 28.3,
		"lastUpdatedOn" : "2016-09-11T12:23:47.672Z",
		"createdOn" : "2015-06-01T17:47:43.344Z",
		"lastPrice" : 71.13,
		"mosPrice" : 61.25,
		"paybackTime" : 16.43,
		"growthRate" : 20.82,
		"predictabilityRating" : 74.223928303988558,
		"linearRegression" : {
			"rSquared" : 1.6538,
			"yIntercept" : 22.2082,
			"slope" : -0.0636
		},
		"marginOfSafety" : -13.89,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "EVC",
		"name" : "Entravision Communications Corp",
		"fiveYearAnalystGrowthEstimate" : 21.5,
		"historicalPeRatio" : 17.9,
		"lastUpdatedOn" : "2016-09-11T12:31:39.406Z",
		"createdOn" : "2015-06-01T17:51:31.863Z",
		"lastPrice" : 7.38,
		"mosPrice" : 8.94,
		"paybackTime" : 14.14,
		"growthRate" : 57.8,
		"predictabilityRating" : -165.434269499391,
		"linearRegression" : {
			"rSquared" : 3.5042,
			"yIntercept" : 20.9395,
			"slope" : -0.0051
		},
		"marginOfSafety" : 21.14,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "EXPR",
		"name" : "Express, Inc. Common Stock",
		"fiveYearAnalystGrowthEstimate" : 12.0,
		"historicalPeRatio" : 14.1,
		"lastUpdatedOn" : "2016-09-11T12:42:15.803Z",
		"createdOn" : "2015-06-01T17:57:02.052Z",
		"lastPrice" : 11.82,
		"mosPrice" : 11.7,
		"paybackTime" : 19.9,
		"growthRate" : 24.23,
		"predictabilityRating" : 48.932531217847774,
		"linearRegression" : {
			"rSquared" : 12.7727,
			"yIntercept" : 11.635,
			"slope" : -0.003
		},
		"marginOfSafety" : -1.02,
		"numberOfFinancialStatements" : 33
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "FB",
		"name" : "Facebook, Inc.",
		"fiveYearAnalystGrowthEstimate" : 35.39,
		"historicalPeRatio" : 0.0,
		"lastUpdatedOn" : "2016-09-11T19:14:34.622Z",
		"createdOn" : "2015-06-01T17:59:25.854Z",
		"lastPrice" : 127.1,
		"mosPrice" : 278.8,
		"paybackTime" : 12.81,
		"growthRate" : 60.77,
		"predictabilityRating" : 25.268761976410019,
		"linearRegression" : {
			"rSquared" : 0.7839,
			"yIntercept" : 6.9618,
			"slope" : -0.0052
		},
		"marginOfSafety" : 119.35,
		"numberOfFinancialStatements" : 23
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "FLIR",
		"name" : "FLIR Systems, Inc.",
		"fiveYearAnalystGrowthEstimate" : 18.450001,
		"historicalPeRatio" : 20.9,
		"lastUpdatedOn" : "2016-09-11T19:49:17.007Z",
		"createdOn" : "2015-06-01T18:12:09.644Z",
		"lastPrice" : 30.7,
		"mosPrice" : 24.84,
		"paybackTime" : 19.47,
		"growthRate" : 16.31,
		"predictabilityRating" : 82.8845601526332,
		"linearRegression" : {
			"rSquared" : 46.4701,
			"yIntercept" : 28.8409,
			"slope" : -0.4666
		},
		"marginOfSafety" : -19.09,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "FORTY",
		"name" : "Formula Systems (1985) Ltd.",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 9.2,
		"lastUpdatedOn" : "2015-07-01T16:51:10.42Z",
		"createdOn" : "2015-06-01T18:18:44.135Z",
		"lastPrice" : 27.01,
		"mosPrice" : 697.67,
		"paybackTime" : 9.99,
		"growthRate" : 41.72,
		"predictabilityRating" : -34.052446991800565,
		"linearRegression" : {
			"rSquared" : 7.0721,
			"yIntercept" : 19.1044,
			"slope" : -0.0044
		},
		"marginOfSafety" : 2483.01,
		"numberOfFinancialStatements" : 44
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "GGAL",
		"name" : "Grupo Financiero Galicia S.A.",
		"fiveYearAnalystGrowthEstimate" : 23.330000000000002,
		"historicalPeRatio" : 6.0,
		"lastUpdatedOn" : "2016-09-11T20:54:05.035Z",
		"createdOn" : "2015-06-01T18:36:50.653Z",
		"lastPrice" : 29.04,
		"mosPrice" : 167.61,
		"paybackTime" : 6.76,
		"growthRate" : 25.94,
		"predictabilityRating" : 42.928213221383231,
		"linearRegression" : {
			"rSquared" : 18.2529,
			"yIntercept" : 18.8528,
			"slope" : 0.0587
		},
		"marginOfSafety" : 477.17,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "GLW",
		"name" : "Corning Incorporated Common Sto",
		"fiveYearAnalystGrowthEstimate" : 15.0,
		"historicalPeRatio" : 11.7,
		"lastUpdatedOn" : "2016-09-11T21:13:39.317Z",
		"createdOn" : "2015-06-01T18:44:26.238Z",
		"lastPrice" : 22.38,
		"mosPrice" : 24.9,
		"paybackTime" : 23.75,
		"growthRate" : 18.11,
		"predictabilityRating" : 59.8265818889247,
		"linearRegression" : {
			"rSquared" : 11.3938,
			"yIntercept" : 23.0416,
			"slope" : -0.0947
		},
		"marginOfSafety" : 11.26,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "GMCR",
		"name" : "Keurig Green Mountain, Inc.",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 35.8,
		"lastUpdatedOn" : "2016-09-11T21:15:01.329Z",
		"createdOn" : "2015-06-01T18:44:50.685Z",
		"lastPrice" : 91.67,
		"mosPrice" : 483.5,
		"paybackTime" : 10.78,
		"growthRate" : 34.26,
		"predictabilityRating" : 48.016562911761504,
		"linearRegression" : {
			"rSquared" : 0.7362,
			"yIntercept" : 19.8727,
			"slope" : -0.012
		},
		"marginOfSafety" : 427.44,
		"numberOfFinancialStatements" : 47
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "GNE",
		"name" : "Genie Energy Ltd. Class B Commo",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 0.0,
		"lastUpdatedOn" : "2016-09-11T21:17:24.142Z",
		"createdOn" : "2015-06-01T18:46:10.847Z",
		"lastPrice" : 6.01,
		"mosPrice" : 312.78,
		"paybackTime" : "NaN",
		"growthRate" : 54.09,
		"predictabilityRating" : -57.462491155997611,
		"linearRegression" : {
			"rSquared" : 3.3886,
			"yIntercept" : 7.4699,
			"slope" : -0.0031
		},
		"marginOfSafety" : 5104.33,
		"numberOfFinancialStatements" : 25
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "GRFS",
		"name" : "Grifols, S.A.",
		"fiveYearAnalystGrowthEstimate" : 15.0,
		"historicalPeRatio" : 0.0,
		"lastUpdatedOn" : "2016-08-08T19:36:33.079Z",
		"createdOn" : "2015-06-01T18:52:12.338Z",
		"lastPrice" : 16.545,
		"mosPrice" : 16.35,
		"paybackTime" : 32.35,
		"growthRate" : 27.26,
		"predictabilityRating" : 32.432175457301895,
		"linearRegression" : {
			"rSquared" : 59.4429,
			"yIntercept" : 10.7892,
			"slope" : -0.0574
		},
		"marginOfSafety" : -1.18,
		"numberOfFinancialStatements" : 26
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "GTN.A",
		"name" : "Gray Television, Inc. CLass A C",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 19.1,
		"lastUpdatedOn" : "2016-08-08T19:25:05.087Z",
		"createdOn" : "2015-06-07T03:04:27.1Z",
		"lastPrice" : 10.65,
		"mosPrice" : 24.88,
		"paybackTime" : 11.38,
		"growthRate" : 22.47,
		"predictabilityRating" : 3.0662184856761883,
		"linearRegression" : {
			"rSquared" : 0.4335,
			"yIntercept" : 20.142,
			"slope" : -0.0018
		},
		"marginOfSafety" : 133.62,
		"numberOfFinancialStatements" : 48
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "GURE",
		"name" : "Gulf Resources, Inc.",
		"fiveYearAnalystGrowthEstimate" : 19.0,
		"historicalPeRatio" : 3.4,
		"lastUpdatedOn" : "2016-08-08T19:21:39.999Z",
		"createdOn" : "2015-06-01T18:58:40.329Z",
		"lastPrice" : 1.74,
		"mosPrice" : 6.37,
		"paybackTime" : 5.77,
		"growthRate" : 11.46,
		"predictabilityRating" : 24.069957729835707,
		"linearRegression" : {
			"rSquared" : 0.1473,
			"yIntercept" : 18.4197,
			"slope" : 0.0006
		},
		"marginOfSafety" : 266.09,
		"numberOfFinancialStatements" : 48
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "HCI",
		"name" : "HCI Group, Inc. Common Stock",
		"fiveYearAnalystGrowthEstimate" : 25.0,
		"historicalPeRatio" : 7.9,
		"lastUpdatedOn" : "2016-08-08T19:05:15.502Z",
		"createdOn" : "2015-06-01T19:05:26.489Z",
		"lastPrice" : 31.97,
		"mosPrice" : 171.51,
		"paybackTime" : 10.06,
		"growthRate" : 26.63,
		"predictabilityRating" : 46.989540568950837,
		"linearRegression" : {
			"rSquared" : 0.0708,
			"yIntercept" : 14.5529,
			"slope" : -0.0003
		},
		"marginOfSafety" : 436.47,
		"numberOfFinancialStatements" : 37
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "HIFS",
		"name" : "Hingham Institution for Savings",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 10.6,
		"lastUpdatedOn" : "2016-08-08T18:50:16.31Z",
		"createdOn" : "2015-06-01T19:12:04.75Z",
		"lastPrice" : 133.9,
		"mosPrice" : 139.77,
		"paybackTime" : 15.25,
		"growthRate" : 15.45,
		"predictabilityRating" : 67.329708632028058,
		"linearRegression" : {
			"rSquared" : 0.878,
			"yIntercept" : 23.688,
			"slope" : -0.0124
		},
		"marginOfSafety" : 4.38,
		"numberOfFinancialStatements" : 55
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "HNNA",
		"name" : "Hennessy Advisors, Inc.",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 16.3,
		"lastUpdatedOn" : "2016-08-08T18:37:48.991Z",
		"createdOn" : "2015-06-01T19:17:22.776Z",
		"lastPrice" : 36.5,
		"mosPrice" : 49.52,
		"paybackTime" : 15.22,
		"growthRate" : 16.92,
		"predictabilityRating" : 45.045552506340918,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 20.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 35.67,
		"numberOfFinancialStatements" : 48
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "HNP",
		"name" : "Huaneng Power Intl Common Stock",
		"fiveYearAnalystGrowthEstimate" : 13.100000000000001,
		"historicalPeRatio" : 15.8,
		"lastUpdatedOn" : "2016-08-08T18:37:19.391Z",
		"createdOn" : "2015-06-01T19:17:34.524Z",
		"lastPrice" : 24.11,
		"mosPrice" : 49.13,
		"paybackTime" : 11.82,
		"growthRate" : 27.33,
		"predictabilityRating" : 13.429718125120417,
		"linearRegression" : {
			"rSquared" : 1.9176,
			"yIntercept" : 19.7671,
			"slope" : 0.0131
		},
		"marginOfSafety" : 103.77,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "HNRG",
		"name" : "Hallador Energy Company",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 14.9,
		"lastUpdatedOn" : "2016-08-08T18:36:16.082Z",
		"createdOn" : "2015-06-01T19:18:00.856Z",
		"lastPrice" : 5.238,
		"mosPrice" : 27.08,
		"paybackTime" : 4.32,
		"growthRate" : 22.88,
		"predictabilityRating" : 14.610420015086135,
		"linearRegression" : {
			"rSquared" : 0.4416,
			"yIntercept" : 20.0233,
			"slope" : -0.0036
		},
		"marginOfSafety" : 416.99,
		"numberOfFinancialStatements" : 48
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "HQCL",
		"name" : "Hanwha Q CELLS Co., Ltd.",
		"fiveYearAnalystGrowthEstimate" : 70.0,
		"historicalPeRatio" : 3.8,
		"lastUpdatedOn" : "2016-08-08T18:26:28.183Z",
		"createdOn" : "2015-06-01T19:21:58.414Z",
		"lastPrice" : 13.36,
		"mosPrice" : 15.31,
		"paybackTime" : 13.52,
		"growthRate" : 12.68,
		"predictabilityRating" : 8.484052744025675,
		"linearRegression" : {
			"rSquared" : 14.0696,
			"yIntercept" : 19.3054,
			"slope" : -0.0213
		},
		"marginOfSafety" : 14.6,
		"numberOfFinancialStatements" : 46
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "IDCC",
		"name" : "InterDigital, Inc.",
		"fiveYearAnalystGrowthEstimate" : 27.1,
		"historicalPeRatio" : 18.4,
		"lastUpdatedOn" : "2016-08-08T17:51:49.867Z",
		"createdOn" : "2015-06-01T19:35:55.657Z",
		"lastPrice" : 66.05,
		"mosPrice" : 260.15,
		"paybackTime" : 9.16,
		"growthRate" : 37.62,
		"predictabilityRating" : -5.3912540482848783,
		"linearRegression" : {
			"rSquared" : 1.2446,
			"yIntercept" : 20.7094,
			"slope" : -0.0026
		},
		"marginOfSafety" : 293.87,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "IGTE",
		"name" : "iGATE Corporation",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 22.4,
		"lastUpdatedOn" : "2016-08-08T17:42:27.15Z",
		"createdOn" : "2015-06-01T19:40:21.605Z",
		"lastPrice" : 48.0,
		"mosPrice" : 45.42,
		"paybackTime" : 26.25,
		"growthRate" : 21.82,
		"predictabilityRating" : 49.024259828528116,
		"linearRegression" : {
			"rSquared" : 4.2905,
			"yIntercept" : 18.4266,
			"slope" : -0.0027
		},
		"marginOfSafety" : -5.37,
		"numberOfFinancialStatements" : 44
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "INTL",
		"name" : "INTL FCStone Inc.",
		"fiveYearAnalystGrowthEstimate" : 19.0,
		"historicalPeRatio" : 19.6,
		"lastUpdatedOn" : "2016-08-08T17:18:27.158Z",
		"createdOn" : "2015-06-01T19:50:09.379Z",
		"lastPrice" : 31.52,
		"mosPrice" : 82.64,
		"paybackTime" : "NaN",
		"growthRate" : 36.07,
		"predictabilityRating" : -37.232214185990614,
		"linearRegression" : {
			"rSquared" : 0.0152,
			"yIntercept" : 20.4947,
			"slope" : 0.0003
		},
		"marginOfSafety" : 162.18,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "ISDR",
		"name" : "Issuer Direct Corporation Commo",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 121.6,
		"lastUpdatedOn" : "2016-08-08T16:59:02.695Z",
		"createdOn" : "2015-06-01T21:12:47.392Z",
		"lastPrice" : 7.28,
		"mosPrice" : 20.33,
		"paybackTime" : 9.06,
		"growthRate" : 33.26,
		"predictabilityRating" : -63.157574001497267,
		"linearRegression" : {
			"rSquared" : 6.1561,
			"yIntercept" : 14.2877,
			"slope" : 0.0068
		},
		"marginOfSafety" : 179.26,
		"numberOfFinancialStatements" : 38
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "JAZZ",
		"name" : "Jazz Pharmaceuticals plc",
		"fiveYearAnalystGrowthEstimate" : 18.270001,
		"historicalPeRatio" : 59.2,
		"lastUpdatedOn" : "2016-08-08T16:44:05.003Z",
		"createdOn" : "2015-06-01T21:19:26.684Z",
		"lastPrice" : 149.57,
		"mosPrice" : 120.51,
		"paybackTime" : 17.31,
		"growthRate" : 17.83,
		"predictabilityRating" : 12.235378541748688,
		"linearRegression" : {
			"rSquared" : 6.4614,
			"yIntercept" : 18.0661,
			"slope" : 0.0237
		},
		"marginOfSafety" : -19.43,
		"numberOfFinancialStatements" : 45
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "JOBS",
		"name" : "51job, Inc.",
		"fiveYearAnalystGrowthEstimate" : 20.0,
		"historicalPeRatio" : 26.5,
		"lastUpdatedOn" : "2016-03-04T07:52:41.094Z",
		"createdOn" : "2015-06-01T21:25:26.081Z",
		"lastPrice" : 32.89,
		"mosPrice" : 41.02,
		"paybackTime" : 15.76,
		"growthRate" : 22.29,
		"predictabilityRating" : 68.443442211853665,
		"linearRegression" : {
			"rSquared" : 0.7921,
			"yIntercept" : 19.8296,
			"slope" : -0.0241
		},
		"marginOfSafety" : 24.72,
		"numberOfFinancialStatements" : 46
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "KEP",
		"name" : "Korea Electric Power Corporatio",
		"fiveYearAnalystGrowthEstimate" : 25.0,
		"historicalPeRatio" : 41.5,
		"lastUpdatedOn" : "2016-08-08T16:21:42.394Z",
		"createdOn" : "2015-06-01T21:31:32.776Z",
		"lastPrice" : 27.46,
		"mosPrice" : 206.61,
		"paybackTime" : 13.37,
		"growthRate" : 251.4,
		"predictabilityRating" : -1532.01202338008,
		"linearRegression" : {
			"rSquared" : 30.4564,
			"yIntercept" : 22.7592,
			"slope" : -0.0021
		},
		"marginOfSafety" : 652.4,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "LAD",
		"name" : "Lithia Motors, Inc. Common Stoc",
		"fiveYearAnalystGrowthEstimate" : 25.0,
		"historicalPeRatio" : 16.6,
		"lastUpdatedOn" : "2016-08-08T16:50:22.582Z",
		"createdOn" : "2015-06-01T21:44:23.251Z",
		"lastPrice" : 84.3,
		"mosPrice" : 70.12,
		"paybackTime" : 35.78,
		"growthRate" : 12.49,
		"predictabilityRating" : 19.205093681513631,
		"linearRegression" : {
			"rSquared" : 2.7228,
			"yIntercept" : 20.2855,
			"slope" : 0.0137
		},
		"marginOfSafety" : -16.82,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "LBMH",
		"name" : "Liberator Medical Holdings, Inc",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 32.9,
		"lastUpdatedOn" : "2016-08-08T16:55:14.394Z",
		"createdOn" : "2015-06-01T21:46:38.269Z",
		"lastPrice" : 3.35,
		"mosPrice" : 44.41,
		"paybackTime" : 11.24,
		"growthRate" : 41.08,
		"predictabilityRating" : -99.026153138472154,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 1225.67,
		"numberOfFinancialStatements" : 46
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "LCI",
		"name" : "Lannett Co Inc Common Stock",
		"fiveYearAnalystGrowthEstimate" : 21.110001,
		"historicalPeRatio" : 57.1,
		"lastUpdatedOn" : "2016-08-08T16:57:50.548Z",
		"createdOn" : "2015-06-01T21:47:46.733Z",
		"lastPrice" : 32.48,
		"mosPrice" : 71.2,
		"paybackTime" : 11.3,
		"growthRate" : 41.0,
		"predictabilityRating" : -70.829717022536784,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 119.21,
		"numberOfFinancialStatements" : 48
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "LGND",
		"name" : "Ligand Pharmaceuticals Incorpor",
		"fiveYearAnalystGrowthEstimate" : 45.910000000000004,
		"historicalPeRatio" : 112.2,
		"lastUpdatedOn" : "2016-08-08T17:10:12.978Z",
		"createdOn" : "2015-06-01T21:53:06.494Z",
		"lastPrice" : 119.5,
		"mosPrice" : 220.48,
		"paybackTime" : 32.83,
		"growthRate" : 16.26,
		"predictabilityRating" : -48.27556369813604,
		"linearRegression" : {
			"rSquared" : 11.6729,
			"yIntercept" : 19.9174,
			"slope" : 0.009
		},
		"marginOfSafety" : 84.5,
		"numberOfFinancialStatements" : 48
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "LKQ",
		"name" : "LKQ Corporation",
		"fiveYearAnalystGrowthEstimate" : 26.75,
		"historicalPeRatio" : 24.1,
		"lastUpdatedOn" : "2016-08-08T17:15:59.841Z",
		"createdOn" : "2015-06-01T21:55:41.489Z",
		"lastPrice" : 34.955,
		"mosPrice" : 79.26,
		"paybackTime" : 13.92,
		"growthRate" : 26.91,
		"predictabilityRating" : 79.533582797253374,
		"linearRegression" : {
			"rSquared" : 17.5441,
			"yIntercept" : 25.6197,
			"slope" : -0.1482
		},
		"marginOfSafety" : 126.75,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "MCOX",
		"name" : "Mecox Lane Limited",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 52.4,
		"lastUpdatedOn" : "2016-03-04T08:29:53.282Z",
		"createdOn" : "2015-06-01T22:18:10.703Z",
		"lastPrice" : 3.795,
		"mosPrice" : 14.39,
		"paybackTime" : "NaN",
		"growthRate" : 25.34,
		"predictabilityRating" : -113.09000071773042,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 279.18,
		"numberOfFinancialStatements" : 27
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "MED",
		"name" : "MEDIFAST INC Common Stock",
		"fiveYearAnalystGrowthEstimate" : 17.0,
		"historicalPeRatio" : 18.2,
		"lastUpdatedOn" : "2016-08-08T18:20:15.616Z",
		"createdOn" : "2015-06-01T22:23:01.487Z",
		"lastPrice" : 35.045,
		"mosPrice" : 33.33,
		"paybackTime" : 17.93,
		"growthRate" : 18.2,
		"predictabilityRating" : 65.658277358292,
		"linearRegression" : {
			"rSquared" : 4.714,
			"yIntercept" : 20.4322,
			"slope" : -0.0133
		},
		"marginOfSafety" : -4.89,
		"numberOfFinancialStatements" : 48
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "MEET",
		"name" : "MeetMe, Inc.",
		"fiveYearAnalystGrowthEstimate" : 20.0,
		"historicalPeRatio" : 79.1,
		"lastUpdatedOn" : "2016-08-08T18:20:44.527Z",
		"createdOn" : "2015-06-01T22:23:14.701Z",
		"lastPrice" : 7.3099,
		"mosPrice" : 19.9,
		"paybackTime" : 19.22,
		"growthRate" : 21.66,
		"predictabilityRating" : -27.942544524292813,
		"linearRegression" : {
			"rSquared" : 1.9507,
			"yIntercept" : 20.0246,
			"slope" : 0.0043
		},
		"marginOfSafety" : 172.23,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "MELI",
		"name" : "MercadoLibre, Inc.",
		"fiveYearAnalystGrowthEstimate" : 24.270001,
		"historicalPeRatio" : 55.0,
		"lastUpdatedOn" : "2016-08-08T18:22:49.254Z",
		"createdOn" : "2015-06-01T22:24:19.21Z",
		"lastPrice" : 171.12,
		"mosPrice" : 155.44,
		"paybackTime" : 19.32,
		"growthRate" : 42.23,
		"predictabilityRating" : 41.43347092005348,
		"linearRegression" : {
			"rSquared" : 30.8914,
			"yIntercept" : 24.4162,
			"slope" : -0.103
		},
		"marginOfSafety" : -9.16,
		"numberOfFinancialStatements" : 48
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "MIDD",
		"name" : "The Middleby Corporation",
		"fiveYearAnalystGrowthEstimate" : 22.0,
		"historicalPeRatio" : 25.3,
		"lastUpdatedOn" : "2016-08-08T18:39:06.953Z",
		"createdOn" : "2015-06-01T22:31:39.278Z",
		"lastPrice" : 121.4,
		"mosPrice" : 144.99,
		"paybackTime" : 17.42,
		"growthRate" : 26.03,
		"predictabilityRating" : 80.5508748097333,
		"linearRegression" : {
			"rSquared" : 24.5714,
			"yIntercept" : 27.111,
			"slope" : -0.2362
		},
		"marginOfSafety" : 19.43,
		"numberOfFinancialStatements" : 48
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "MOH",
		"name" : "Molina Healthcare Inc Common St",
		"fiveYearAnalystGrowthEstimate" : 17.65,
		"historicalPeRatio" : 62.8,
		"lastUpdatedOn" : "2016-08-08T19:02:31.133Z",
		"createdOn" : "2015-06-01T23:26:42.231Z",
		"lastPrice" : 58.58,
		"mosPrice" : 50.76,
		"paybackTime" : 10.47,
		"growthRate" : 17.81,
		"predictabilityRating" : 54.1839199171749,
		"linearRegression" : {
			"rSquared" : 1.7936,
			"yIntercept" : 19.8065,
			"slope" : 0.0223
		},
		"marginOfSafety" : -13.35,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "MPEL",
		"name" : "Melco Crown Entertainment Limit",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 59.1,
		"lastUpdatedOn" : "2016-03-04T08:43:25.335Z",
		"createdOn" : "2015-06-01T23:28:19.869Z",
		"lastPrice" : 16.655,
		"mosPrice" : 779.72,
		"paybackTime" : "Infinity",
		"growthRate" : 71.53,
		"predictabilityRating" : -67.8856983296171,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 4581.6,
		"numberOfFinancialStatements" : 46
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "MY",
		"name" : "China Ming Yang Wind Power Grou",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 6.8,
		"lastUpdatedOn" : "2016-03-04T08:53:46.839Z",
		"createdOn" : "2015-06-01T23:42:26.837Z",
		"lastPrice" : 2.41,
		"mosPrice" : 6.52,
		"paybackTime" : 1.26,
		"growthRate" : 16.85,
		"predictabilityRating" : -79.756442500924663,
		"linearRegression" : {
			"rSquared" : 0.0438,
			"yIntercept" : 8.5027,
			"slope" : -0.0007
		},
		"marginOfSafety" : 170.54,
		"numberOfFinancialStatements" : 27
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "NHTC",
		"name" : "NATURAL HEALTH TREND",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 9.2,
		"lastUpdatedOn" : "2016-09-11T20:58:41.379Z",
		"createdOn" : "2015-06-01T23:55:21.381Z",
		"lastPrice" : 28.91,
		"mosPrice" : 216.75,
		"paybackTime" : 7.15,
		"growthRate" : 23.85,
		"predictabilityRating" : -22.1347106522298,
		"linearRegression" : {
			"rSquared" : 5.9505,
			"yIntercept" : 20.6219,
			"slope" : 0.0104
		},
		"marginOfSafety" : 649.74,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "NOAH",
		"name" : "Noah Holdings Limited",
		"fiveYearAnalystGrowthEstimate" : 21.0,
		"historicalPeRatio" : 31.1,
		"lastUpdatedOn" : "2016-09-11T20:47:04.392Z",
		"createdOn" : "2015-06-01T23:59:57.517Z",
		"lastPrice" : 26.34,
		"mosPrice" : 56.57,
		"paybackTime" : "Infinity",
		"growthRate" : 52.85,
		"predictabilityRating" : 46.448002250021538,
		"linearRegression" : {
			"rSquared" : 21.5287,
			"yIntercept" : 11.3323,
			"slope" : -0.0255
		},
		"marginOfSafety" : 114.77,
		"numberOfFinancialStatements" : 28
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "NVEC",
		"name" : "NVE Corporation",
		"fiveYearAnalystGrowthEstimate" : 25.0,
		"historicalPeRatio" : 22.6,
		"lastUpdatedOn" : "2016-09-11T20:10:19.54Z",
		"createdOn" : "2015-06-02T00:12:02.983Z",
		"lastPrice" : 59.02,
		"mosPrice" : 74.63,
		"paybackTime" : 18.86,
		"growthRate" : 20.15,
		"predictabilityRating" : 75.5472731980266,
		"linearRegression" : {
			"rSquared" : 30.2166,
			"yIntercept" : 23.3403,
			"slope" : -0.0523
		},
		"marginOfSafety" : 26.45,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "NVEE",
		"name" : "NV5 Global, Inc.",
		"fiveYearAnalystGrowthEstimate" : 20.0,
		"historicalPeRatio" : 0.0,
		"lastUpdatedOn" : "2016-09-11T20:09:34.53Z",
		"createdOn" : "2015-06-02T00:12:04.306Z",
		"lastPrice" : 29.71,
		"mosPrice" : 39.18,
		"paybackTime" : 17.6,
		"growthRate" : 28.66,
		"predictabilityRating" : 14.697051340531019,
		"linearRegression" : {
			"rSquared" : 5.7662,
			"yIntercept" : 5.3662,
			"slope" : 0.0055
		},
		"marginOfSafety" : 31.87,
		"numberOfFinancialStatements" : 21
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "NVGS",
		"name" : "Navigator Holdings Ltd. Ordinar",
		"fiveYearAnalystGrowthEstimate" : 9.0,
		"historicalPeRatio" : 17.6,
		"lastUpdatedOn" : "2016-09-11T20:08:42.658Z",
		"createdOn" : "2015-06-02T00:12:17.812Z",
		"lastPrice" : 6.75,
		"mosPrice" : 7.64,
		"paybackTime" : "NaN",
		"growthRate" : 30.57,
		"predictabilityRating" : 51.564400890346043,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 13.19,
		"numberOfFinancialStatements" : 25
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "NXPI",
		"name" : "NXP Semiconductors N.V.",
		"fiveYearAnalystGrowthEstimate" : 34.0,
		"historicalPeRatio" : 55.3,
		"lastUpdatedOn" : "2016-09-11T20:00:43.212Z",
		"createdOn" : "2015-06-02T00:15:32.025Z",
		"lastPrice" : 81.4,
		"mosPrice" : 277.88,
		"paybackTime" : 13.55,
		"growthRate" : 26.92,
		"predictabilityRating" : -23.668156037223497,
		"linearRegression" : {
			"rSquared" : 6.5852,
			"yIntercept" : 10.5474,
			"slope" : 0.0124
		},
		"marginOfSafety" : 241.38,
		"numberOfFinancialStatements" : 31
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "ONE",
		"name" : "Higher One Holdings, Inc.",
		"fiveYearAnalystGrowthEstimate" : 30.0,
		"historicalPeRatio" : 0.0,
		"lastUpdatedOn" : "2016-09-11T19:27:20.755Z",
		"createdOn" : "2015-06-02T00:27:44.141Z",
		"lastPrice" : 5.15,
		"mosPrice" : 4.87,
		"paybackTime" : "NaN",
		"growthRate" : 19.95,
		"predictabilityRating" : 45.226673608857588,
		"linearRegression" : {
			"rSquared" : 15.4808,
			"yIntercept" : 11.5097,
			"slope" : -0.0095
		},
		"marginOfSafety" : -5.44,
		"numberOfFinancialStatements" : 30
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "OTEL",
		"name" : "Otelco Inc.",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 0.0,
		"lastUpdatedOn" : "2016-09-11T16:44:53.863Z",
		"createdOn" : "2015-06-02T00:33:33.156Z",
		"lastPrice" : 4.46,
		"mosPrice" : 12.19,
		"paybackTime" : 1.64,
		"growthRate" : 9.71,
		"predictabilityRating" : -35.332919318614813,
		"linearRegression" : {
			"rSquared" : 0.2201,
			"yIntercept" : 20.4865,
			"slope" : -0.0009
		},
		"marginOfSafety" : 173.32,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "PBHC",
		"name" : "Pathfinder Bancorp, Inc.",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 15.3,
		"lastUpdatedOn" : "2016-09-11T12:32:14.448Z",
		"createdOn" : "2015-06-02T00:39:45.005Z",
		"lastPrice" : 12.0766,
		"mosPrice" : 13.77,
		"paybackTime" : "NaN",
		"growthRate" : 16.53,
		"predictabilityRating" : 56.831510891067182,
		"linearRegression" : {
			"rSquared" : 0.3459,
			"yIntercept" : 20.2715,
			"slope" : 0.0047
		},
		"marginOfSafety" : 14.02,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "PKBK",
		"name" : "Parke Bancorp, Inc.",
		"fiveYearAnalystGrowthEstimate" : 26.299998000000002,
		"historicalPeRatio" : 7.1,
		"lastUpdatedOn" : "2016-09-11T11:59:40.866Z",
		"createdOn" : "2015-06-02T00:55:33.905Z",
		"lastPrice" : 15.1201,
		"mosPrice" : 25.56,
		"paybackTime" : 35.53,
		"growthRate" : 15.17,
		"predictabilityRating" : 70.01871149550756,
		"linearRegression" : {
			"rSquared" : 5.9861,
			"yIntercept" : 22.3636,
			"slope" : -0.0749
		},
		"marginOfSafety" : 69.05,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "PRAA",
		"name" : "PRA Group, Inc.",
		"fiveYearAnalystGrowthEstimate" : 15.0,
		"historicalPeRatio" : 15.5,
		"lastUpdatedOn" : "2016-09-11T11:31:38.942Z",
		"createdOn" : "2015-06-02T01:09:20.876Z",
		"lastPrice" : 32.03,
		"mosPrice" : 40.5,
		"paybackTime" : 16.2,
		"growthRate" : 18.66,
		"predictabilityRating" : 88.424150083095356,
		"linearRegression" : {
			"rSquared" : 4.6851,
			"yIntercept" : 24.9959,
			"slope" : -0.2418
		},
		"marginOfSafety" : 26.44,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "PRLB",
		"name" : "Proto Labs, Inc. Common stock",
		"fiveYearAnalystGrowthEstimate" : 27.500000000000004,
		"historicalPeRatio" : 0.0,
		"lastUpdatedOn" : "2016-09-11T11:26:53.759Z",
		"createdOn" : "2015-06-02T01:11:28.295Z",
		"lastPrice" : 52.84,
		"mosPrice" : 96.46,
		"paybackTime" : 18.36,
		"growthRate" : 33.05,
		"predictabilityRating" : 24.279965830856426,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 82.55,
		"numberOfFinancialStatements" : 27
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "PWRD",
		"name" : "Perfect World Co., Ltd.",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 9.3,
		"lastUpdatedOn" : "2016-09-11T11:11:18.902Z",
		"createdOn" : "2015-06-02T01:20:26.64Z",
		"lastPrice" : 20.17,
		"mosPrice" : 496.53,
		"paybackTime" : 5.93,
		"growthRate" : 42.57,
		"predictabilityRating" : 13.585983083609818,
		"linearRegression" : {
			"rSquared" : 34.5879,
			"yIntercept" : 16.4364,
			"slope" : -0.0193
		},
		"marginOfSafety" : 2361.73,
		"numberOfFinancialStatements" : 37
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "QIHU",
		"name" : "Qihoo 360 Technology Co. Ltd. A",
		"fiveYearAnalystGrowthEstimate" : 38.759997,
		"historicalPeRatio" : 0.0,
		"lastUpdatedOn" : "2016-09-11T11:03:35.087Z",
		"createdOn" : "2015-06-02T01:23:44.61Z",
		"lastPrice" : 76.92,
		"mosPrice" : 413.27,
		"paybackTime" : 10.6,
		"growthRate" : 46.25,
		"predictabilityRating" : -4.9500960394779838,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 437.27,
		"numberOfFinancialStatements" : 24
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "RGR",
		"name" : "Sturm, Ruger & Company, Inc. Co",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 17.9,
		"lastUpdatedOn" : "2016-09-11T10:30:44.069Z",
		"createdOn" : "2015-06-02T01:39:24.109Z",
		"lastPrice" : 57.88,
		"mosPrice" : 269.18,
		"paybackTime" : 11.53,
		"growthRate" : 26.47,
		"predictabilityRating" : 19.092141360616964,
		"linearRegression" : {
			"rSquared" : 2.4143,
			"yIntercept" : 20.9778,
			"slope" : -0.0031
		},
		"marginOfSafety" : 365.07,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "RTK",
		"name" : "Rentech, Inc.",
		"fiveYearAnalystGrowthEstimate" : 15.0,
		"historicalPeRatio" : 75.6,
		"lastUpdatedOn" : "2016-09-11T09:59:30.247Z",
		"createdOn" : "2015-06-02T01:53:18.304Z",
		"lastPrice" : 3.61,
		"mosPrice" : 145.2,
		"paybackTime" : 12.38,
		"growthRate" : 50.35,
		"predictabilityRating" : -87.087574913496042,
		"linearRegression" : {
			"rSquared" : 0.2687,
			"yIntercept" : 20.4235,
			"slope" : 0.0015
		},
		"marginOfSafety" : 3922.16,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "SCLN",
		"name" : "SciClone Pharmaceuticals, Inc.",
		"fiveYearAnalystGrowthEstimate" : 14.099999999999998,
		"historicalPeRatio" : 18.3,
		"lastUpdatedOn" : "2016-09-11T09:35:39.473Z",
		"createdOn" : "2015-06-02T02:04:41.674Z",
		"lastPrice" : 10.09,
		"mosPrice" : 9.65,
		"paybackTime" : 20.91,
		"growthRate" : 14.72,
		"predictabilityRating" : 29.688384062700678,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 20.5,
			"slope" : 0.0
		},
		"marginOfSafety" : -4.36,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "SENEA",
		"name" : "Seneca Foods Corp.",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 26.5,
		"lastUpdatedOn" : "2016-09-11T09:26:38.607Z",
		"createdOn" : "2015-06-02T02:08:49.906Z",
		"lastPrice" : 29.36,
		"mosPrice" : 86.96,
		"paybackTime" : "NaN",
		"growthRate" : 15.82,
		"predictabilityRating" : 29.898545953071192,
		"linearRegression" : {
			"rSquared" : 5.9222,
			"yIntercept" : 21.5005,
			"slope" : -0.0006
		},
		"marginOfSafety" : 196.19,
		"numberOfFinancialStatements" : 50
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "SENEB",
		"name" : "Seneca Foods Corp.",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 28.2,
		"lastUpdatedOn" : "2016-09-11T09:26:07.704Z",
		"createdOn" : "2015-06-02T02:09:01.185Z",
		"lastPrice" : 39.0,
		"mosPrice" : 86.96,
		"paybackTime" : "NaN",
		"growthRate" : 15.82,
		"predictabilityRating" : 29.898545953071192,
		"linearRegression" : {
			"rSquared" : 5.9222,
			"yIntercept" : 21.5005,
			"slope" : -0.0006
		},
		"marginOfSafety" : 122.97,
		"numberOfFinancialStatements" : 50
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "SFBC",
		"name" : "Sound Financial Bancorp, Inc.",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 0.0,
		"lastUpdatedOn" : "2016-09-11T09:24:19.277Z",
		"createdOn" : "2015-06-02T02:09:32.676Z",
		"lastPrice" : 24.5,
		"mosPrice" : 308.24,
		"paybackTime" : 7.18,
		"growthRate" : 34.74,
		"predictabilityRating" : 40.960280107371787,
		"linearRegression" : {
			"rSquared" : 2.0483,
			"yIntercept" : 6.1981,
			"slope" : 0.0036
		},
		"marginOfSafety" : 1158.12,
		"numberOfFinancialStatements" : 23
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "SGBK",
		"name" : "Stonegate Bank",
		"fiveYearAnalystGrowthEstimate" : 17.0,
		"historicalPeRatio" : 20.8,
		"lastUpdatedOn" : "2016-09-11T09:20:30.507Z",
		"createdOn" : "2015-06-02T02:11:20.573Z",
		"lastPrice" : 32.75,
		"mosPrice" : 47.47,
		"paybackTime" : 18.62,
		"growthRate" : 31.15,
		"predictabilityRating" : 4.6266551351254748,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 44.95,
		"numberOfFinancialStatements" : 30
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "SILC",
		"name" : "Silicom Ltd",
		"fiveYearAnalystGrowthEstimate" : 15.0,
		"historicalPeRatio" : 17.8,
		"lastUpdatedOn" : "2016-03-04T10:45:58.809Z",
		"createdOn" : "2015-06-02T02:18:00.535Z",
		"lastPrice" : 30.46,
		"mosPrice" : 29.55,
		"paybackTime" : "Infinity",
		"growthRate" : 19.71,
		"predictabilityRating" : 41.373172569884453,
		"linearRegression" : {
			"rSquared" : 0.8794,
			"yIntercept" : 19.2737,
			"slope" : -0.0038
		},
		"marginOfSafety" : -2.99,
		"numberOfFinancialStatements" : 47
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "SIRI",
		"name" : "Sirius XM Holdings Inc.",
		"fiveYearAnalystGrowthEstimate" : 22.98,
		"historicalPeRatio" : 53.4,
		"lastUpdatedOn" : "2016-09-11T09:00:17.784Z",
		"createdOn" : "2015-06-02T02:18:59.273Z",
		"lastPrice" : 4.1,
		"mosPrice" : 3.62,
		"paybackTime" : 15.64,
		"growthRate" : 19.9,
		"predictabilityRating" : -6.2774437040134217,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 20.5,
			"slope" : 0.0
		},
		"marginOfSafety" : -11.71,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "SIRO",
		"name" : "Sirona Dental Systems, Inc.",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 26.7,
		"lastUpdatedOn" : "2016-09-11T08:59:50.344Z",
		"createdOn" : "2015-06-02T02:19:11.36Z",
		"lastPrice" : 109.97,
		"mosPrice" : 146.21,
		"paybackTime" : 17.03,
		"growthRate" : 22.65,
		"predictabilityRating" : 57.817669949454547,
		"linearRegression" : {
			"rSquared" : 11.4763,
			"yIntercept" : 21.4201,
			"slope" : -0.0485
		},
		"marginOfSafety" : 32.95,
		"numberOfFinancialStatements" : 47
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "SLI",
		"name" : "SL Industries, Inc. Common Stoc",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 10.0,
		"lastUpdatedOn" : "2016-03-04T10:49:12.16Z",
		"createdOn" : "2015-06-02T02:23:39.85Z",
		"lastPrice" : 33.56,
		"mosPrice" : 66.54,
		"paybackTime" : 33.37,
		"growthRate" : 16.26,
		"predictabilityRating" : 62.052820531988608,
		"linearRegression" : {
			"rSquared" : 0.1083,
			"yIntercept" : 18.7902,
			"slope" : 0.0077
		},
		"marginOfSafety" : 98.27,
		"numberOfFinancialStatements" : 46
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "SPLP",
		"name" : "Steel Partners Holdings LP LTD",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 0.0,
		"lastUpdatedOn" : "2016-09-11T08:24:06.623Z",
		"createdOn" : "2015-06-02T02:39:27.086Z",
		"lastPrice" : 14.4,
		"mosPrice" : 256.84,
		"paybackTime" : 5.05,
		"growthRate" : 31.3,
		"predictabilityRating" : -4.6814675445120315,
		"linearRegression" : {
			"rSquared" : 0.0288,
			"yIntercept" : 6.9935,
			"slope" : 0.0003
		},
		"marginOfSafety" : 1683.61,
		"numberOfFinancialStatements" : 23
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "STRS",
		"name" : "Stratus Properties, Inc.",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 40.1,
		"lastUpdatedOn" : "2016-09-11T07:52:27.625Z",
		"createdOn" : "2015-06-02T03:00:49.267Z",
		"lastPrice" : 22.01,
		"mosPrice" : 27.05,
		"paybackTime" : "NaN",
		"growthRate" : 20.85,
		"predictabilityRating" : -26.29212110518516,
		"linearRegression" : {
			"rSquared" : 5.7222,
			"yIntercept" : 21.5253,
			"slope" : -0.022
		},
		"marginOfSafety" : 22.9,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "SURG",
		"name" : "Synergetics USA, Inc.",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 24.6,
		"lastUpdatedOn" : "2016-09-11T07:45:31.867Z",
		"createdOn" : "2015-06-02T03:04:53.861Z",
		"lastPrice" : 6.69,
		"mosPrice" : 8.74,
		"paybackTime" : 14.2,
		"growthRate" : 23.6,
		"predictabilityRating" : 25.549442406007884,
		"linearRegression" : {
			"rSquared" : 2.2626,
			"yIntercept" : 19.7008,
			"slope" : -0.0093
		},
		"marginOfSafety" : 30.64,
		"numberOfFinancialStatements" : 46
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "TCX",
		"name" : "Tucows Inc.",
		"fiveYearAnalystGrowthEstimate" : 28.999999999999996,
		"historicalPeRatio" : 26.0,
		"lastUpdatedOn" : "2016-09-11T07:16:41.429Z",
		"createdOn" : "2015-06-02T03:21:23.559Z",
		"lastPrice" : 27.51,
		"mosPrice" : 53.97,
		"paybackTime" : 19.37,
		"growthRate" : 22.05,
		"predictabilityRating" : 58.675101048198279,
		"linearRegression" : {
			"rSquared" : 0.2181,
			"yIntercept" : 20.7683,
			"slope" : -0.0062
		},
		"marginOfSafety" : 96.18,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "TFM",
		"name" : "The Fresh Market, Inc.",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 45.1,
		"lastUpdatedOn" : "2016-09-11T07:06:00.538Z",
		"createdOn" : "2015-06-02T03:26:05.008Z",
		"lastPrice" : 28.51,
		"mosPrice" : 89.93,
		"paybackTime" : 13.69,
		"growthRate" : 27.54,
		"predictabilityRating" : 60.706755660747163,
		"linearRegression" : {
			"rSquared" : 11.5356,
			"yIntercept" : 10.3354,
			"slope" : -0.0274
		},
		"marginOfSafety" : 215.43,
		"numberOfFinancialStatements" : 29
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "THRM",
		"name" : "Gentherm Inc",
		"fiveYearAnalystGrowthEstimate" : 25.0,
		"historicalPeRatio" : 42.8,
		"lastUpdatedOn" : "2016-09-11T06:56:38.669Z",
		"createdOn" : "2015-06-02T03:30:23.004Z",
		"lastPrice" : 30.71,
		"mosPrice" : 92.08,
		"paybackTime" : 13.71,
		"growthRate" : 50.61,
		"predictabilityRating" : -5.5242693849181705,
		"linearRegression" : {
			"rSquared" : 3.1716,
			"yIntercept" : 20.5591,
			"slope" : 0.0019
		},
		"marginOfSafety" : 199.84,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "TMUS",
		"name" : "T-Mobile US, Inc.",
		"fiveYearAnalystGrowthEstimate" : 36.480000000000004,
		"historicalPeRatio" : 55.0,
		"lastUpdatedOn" : "2016-09-11T06:43:21.509Z",
		"createdOn" : "2015-06-02T03:36:29.35Z",
		"lastPrice" : 44.64,
		"mosPrice" : 262.85,
		"paybackTime" : "NaN",
		"growthRate" : 45.55,
		"predictabilityRating" : -33.221015735362272,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 488.82,
		"numberOfFinancialStatements" : 50
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "TNH",
		"name" : "Terra Nitrogen Company, L.P. Co",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 11.4,
		"lastUpdatedOn" : "2016-09-11T06:41:51.993Z",
		"createdOn" : "2015-06-02T03:37:11.707Z",
		"lastPrice" : 115.64,
		"mosPrice" : 97.44,
		"paybackTime" : 19.92,
		"growthRate" : 12.7,
		"predictabilityRating" : 55.82044971950242,
		"linearRegression" : {
			"rSquared" : 2.7367,
			"yIntercept" : 20.7388,
			"slope" : 0.0032
		},
		"marginOfSafety" : -15.74,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "TREC",
		"name" : "Trecora Resources Common Stock",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 21.6,
		"lastUpdatedOn" : "2016-09-11T06:33:27.922Z",
		"createdOn" : "2015-06-02T03:41:55.89Z",
		"lastPrice" : 10.21,
		"mosPrice" : 42.83,
		"paybackTime" : 25.46,
		"growthRate" : 22.36,
		"predictabilityRating" : -17.130881030192427,
		"linearRegression" : {
			"rSquared" : 3.7351,
			"yIntercept" : 21.5329,
			"slope" : -0.0143
		},
		"marginOfSafety" : 319.49,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "TSM",
		"name" : "Taiwan Semiconductor Manufactur",
		"fiveYearAnalystGrowthEstimate" : 15.0,
		"historicalPeRatio" : 14.9,
		"lastUpdatedOn" : "2016-09-11T06:20:33.63Z",
		"createdOn" : "2015-06-02T03:48:16.577Z",
		"lastPrice" : 28.59,
		"mosPrice" : 25.35,
		"paybackTime" : 22.11,
		"growthRate" : 18.62,
		"predictabilityRating" : 69.881817018553463,
		"linearRegression" : {
			"rSquared" : 12.4074,
			"yIntercept" : 17.3398,
			"slope" : 0.115
		},
		"marginOfSafety" : -11.33,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "TTM",
		"name" : "Tata Motors Ltd Tata Motors Lim",
		"fiveYearAnalystGrowthEstimate" : 20.0,
		"historicalPeRatio" : 9.1,
		"lastUpdatedOn" : "2016-03-04T11:47:46.437Z",
		"createdOn" : "2015-06-02T03:51:38.192Z",
		"lastPrice" : 25.71,
		"mosPrice" : 68.57,
		"paybackTime" : "NaN",
		"growthRate" : 23.06,
		"predictabilityRating" : -12.32611853774435,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 166.71,
		"numberOfFinancialStatements" : 46
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "TV",
		"name" : "Grupo Televisa S.A. Common Stoc",
		"fiveYearAnalystGrowthEstimate" : 17.45,
		"historicalPeRatio" : 38.3,
		"lastUpdatedOn" : "2016-09-11T06:09:54.178Z",
		"createdOn" : "2015-06-02T03:53:13.602Z",
		"lastPrice" : 25.34,
		"mosPrice" : 1586.58,
		"paybackTime" : "NaN",
		"growthRate" : 62.08,
		"predictabilityRating" : -132.38056426205969,
		"linearRegression" : {
			"rSquared" : 3.7154,
			"yIntercept" : 19.3763,
			"slope" : 0.0037
		},
		"marginOfSafety" : 6161.17,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "UMC",
		"name" : "United Microelectronics Corpora",
		"fiveYearAnalystGrowthEstimate" : 20.0,
		"historicalPeRatio" : 15.8,
		"lastUpdatedOn" : "2016-09-11T05:46:06.804Z",
		"createdOn" : "2015-06-02T04:05:03.786Z",
		"lastPrice" : 1.86,
		"mosPrice" : 2.75,
		"paybackTime" : "NaN",
		"growthRate" : 35.87,
		"predictabilityRating" : -92.879351304747644,
		"linearRegression" : {
			"rSquared" : 20.1094,
			"yIntercept" : 22.0087,
			"slope" : -0.0067
		},
		"marginOfSafety" : 47.85,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "UVE",
		"name" : "UNIVERSAL INSURANCE HOLDINGS IN",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 8.2,
		"lastUpdatedOn" : "2016-09-11T05:24:36.466Z",
		"createdOn" : "2015-06-02T04:14:54.283Z",
		"lastPrice" : 24.54,
		"mosPrice" : 2819.95,
		"paybackTime" : 6.87,
		"growthRate" : 52.08,
		"predictabilityRating" : 23.327207449236155,
		"linearRegression" : {
			"rSquared" : 27.8599,
			"yIntercept" : 24.1886,
			"slope" : -0.0515
		},
		"marginOfSafety" : 11391.24,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "VIA",
		"name" : "Viacom Inc.",
		"fiveYearAnalystGrowthEstimate" : 14.7,
		"historicalPeRatio" : 14.9,
		"lastUpdatedOn" : "2016-09-11T05:01:27.332Z",
		"createdOn" : "2015-06-02T04:20:22.313Z",
		"lastPrice" : 41.67,
		"mosPrice" : 40.58,
		"paybackTime" : 22.89,
		"growthRate" : 11.07,
		"predictabilityRating" : 81.3704108002887,
		"linearRegression" : {
			"rSquared" : 30.845,
			"yIntercept" : 22.3646,
			"slope" : -0.2203
		},
		"marginOfSafety" : -2.62,
		"numberOfFinancialStatements" : 48
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "VIMC",
		"name" : "Vimicro International Corporati",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 19.5,
		"lastUpdatedOn" : "2016-09-11T04:54:14.22Z",
		"createdOn" : "2015-06-02T04:21:49.092Z",
		"lastPrice" : 13.43,
		"mosPrice" : 15.13,
		"paybackTime" : "NaN",
		"growthRate" : 25.09,
		"predictabilityRating" : -18.360666936665694,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 12.66,
		"numberOfFinancialStatements" : 44
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "VIPS",
		"name" : "Vipshop Holdings Limited Americ",
		"fiveYearAnalystGrowthEstimate" : 41.8,
		"historicalPeRatio" : 0.0,
		"lastUpdatedOn" : "2016-03-04T12:08:48.385Z",
		"createdOn" : "2015-06-02T04:21:57.769Z",
		"lastPrice" : 10.95,
		"mosPrice" : 88.22,
		"paybackTime" : "Infinity",
		"growthRate" : 36.86,
		"predictabilityRating" : -87.872709206481971,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 705.66,
		"numberOfFinancialStatements" : 22
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "VOYA",
		"name" : "Voya Financial, Inc. Common Sto",
		"fiveYearAnalystGrowthEstimate" : 18.620001,
		"historicalPeRatio" : 0.0,
		"lastUpdatedOn" : "2016-09-11T04:28:20.511Z",
		"createdOn" : "2015-06-02T04:26:12.65Z",
		"lastPrice" : 29.19,
		"mosPrice" : 35.29,
		"paybackTime" : 3.36,
		"growthRate" : 21.41,
		"predictabilityRating" : 25.036145011332934,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 20.9,
		"numberOfFinancialStatements" : 23
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "WHG",
		"name" : "Westwood Holdings Group Inc Com",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 21.2,
		"lastUpdatedOn" : "2016-09-11T02:34:32.627Z",
		"createdOn" : "2015-06-02T04:39:43.015Z",
		"lastPrice" : 50.57,
		"mosPrice" : 48.53,
		"paybackTime" : 15.37,
		"growthRate" : 16.51,
		"predictabilityRating" : 78.188468482409192,
		"linearRegression" : {
			"rSquared" : 0.1685,
			"yIntercept" : 20.5895,
			"slope" : -0.0026
		},
		"marginOfSafety" : -4.03,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "WLFC",
		"name" : "Willis Lease Finance Corporatio",
		"fiveYearAnalystGrowthEstimate" : 15.0,
		"historicalPeRatio" : 18.3,
		"lastUpdatedOn" : "2016-09-11T02:27:20.498Z",
		"createdOn" : "2015-06-02T04:42:24.669Z",
		"lastPrice" : 22.32,
		"mosPrice" : 25.65,
		"paybackTime" : "NaN",
		"growthRate" : 16.5,
		"predictabilityRating" : 43.339420435161664,
		"linearRegression" : {
			"rSquared" : 3.0377,
			"yIntercept" : 20.9739,
			"slope" : -0.0162
		},
		"marginOfSafety" : 14.92,
		"numberOfFinancialStatements" : 49
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "XIN",
		"name" : "Xinyuan Real Estate Co Ltd Amer",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 2.8,
		"lastUpdatedOn" : "2016-03-04T12:29:11.078Z",
		"createdOn" : "2015-06-02T04:55:55.234Z",
		"lastPrice" : 3.99,
		"mosPrice" : 64.75,
		"paybackTime" : "NaN",
		"growthRate" : 26.61,
		"predictabilityRating" : -63.569206356563171,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 1522.81,
		"numberOfFinancialStatements" : 46
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "XNY",
		"name" : "China Xiniya Fashion Limited Am",
		"fiveYearAnalystGrowthEstimate" : 0.0,
		"historicalPeRatio" : 3.3,
		"lastUpdatedOn" : "2016-09-11T01:55:48.176Z",
		"createdOn" : "2015-06-02T04:56:40.949Z",
		"lastPrice" : 1.43,
		"mosPrice" : 2.14,
		"paybackTime" : "NaN",
		"growthRate" : 22.59,
		"predictabilityRating" : -32.914467315920888,
		"linearRegression" : {
			"rSquared" : 31.9382,
			"yIntercept" : 9.2231,
			"slope" : -0.0116
		},
		"marginOfSafety" : 49.65,
		"numberOfFinancialStatements" : 26
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "XRS",
		"name" : "TAL Education Group American De",
		"fiveYearAnalystGrowthEstimate" : 28.78,
		"historicalPeRatio" : 32.6,
		"lastUpdatedOn" : "2016-03-04T12:31:19.679Z",
		"createdOn" : "2015-06-02T04:58:40.371Z",
		"lastPrice" : 50.22,
		"mosPrice" : 109.77,
		"paybackTime" : 14.13,
		"growthRate" : 36.2,
		"predictabilityRating" : 38.503733231434367,
		"linearRegression" : {
			"rSquared" : 3.143,
			"yIntercept" : 8.339,
			"slope" : -0.0015
		},
		"marginOfSafety" : 118.58,
		"numberOfFinancialStatements" : 27
	}, {
		"id" : "000000000000000000000000",
		"stockSymbol" : "YY",
		"name" : "YY Inc.",
		"fiveYearAnalystGrowthEstimate" : 26.200000000000003,
		"historicalPeRatio" : 0.0,
		"lastUpdatedOn" : "2016-09-11T01:42:38.704Z",
		"createdOn" : "2015-06-02T05:01:43.807Z",
		"lastPrice" : 50.11,
		"mosPrice" : 728.65,
		"paybackTime" : "Infinity",
		"growthRate" : 248.84,
		"predictabilityRating" : -365.73187957593512,
		"linearRegression" : {
			"rSquared" : 0.0,
			"yIntercept" : 0.0,
			"slope" : 0.0
		},
		"marginOfSafety" : 1354.1,
		"numberOfFinancialStatements" : 23
	}
]