import {Component, OnInit} from '@angular/core';
import {NavController} from 'ionic-angular';

import {StockScreenerService} from '../../services/stockScreenerService';
import { StockScreenerSearchResult } from '../../models/StockScreenerSearchResult';
import {StockScreenerResultComponent} from '../../controls/stock-screener-result.component'


@Component({
    templateUrl: 'build/pages/stockScreener/stockScreener.html',
    providers: [StockScreenerService],
    directives: [StockScreenerResultComponent]
})
export class StockScreenerPage implements OnInit {
    results: StockScreenerSearchResult [];

    constructor(private navCtrl: NavController, private stockScreenerService: StockScreenerService) {
    }

    ngOnInit(): void {
        this.getUnderValuedStocks();
    }

    getUnderValuedStocks(): void {
        this.results = this.stockScreenerService.getUnderValuedStocks();
    }
}