import {Component} from '@angular/core';
import {StockPage} from '../stock/stock';
import {StockScreenerPage} from '../stockScreener/stockScreener';

@Component({
  templateUrl: 'build/pages/tabs/tabs.html'
})
export class TabsPage {

  private tab1Root: any;
  private tab2Root: any;

  constructor() {
    // this tells the tabs component which Pages
    // should be each tab's root Page
    this.tab1Root = StockPage;
    this.tab2Root = StockScreenerPage;
  }
}
